package com.raizcubica.edebepubmanager;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.input.ReaderInputStream;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.raizcubica.edebepubmanager.model.PublicationItem;
import com.raizcubica.edebepubmanager.model.PublicationObject;
import com.raizcubica.edebepubmanager.model.PublicationPage;



public class PublicationXmlDomParser {
	ArrayList<PublicationPage> pages;
	private BufferedWriter bw;
	

	public PublicationXmlDomParser() {
		super();
		pages = new ArrayList<PublicationPage>();
	}
	
	public void parseFile(File file){
		try{
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setValidating(false);
		dbf.setExpandEntityReferences(true);
		//dbf.setExpandEntityReferences(false); 
		dbf.setFeature("http://xml.org/sax/features/use-entity-resolver2", true);
		dbf.setFeature("http://xml.org/sax/features/validation", false);
		DocumentBuilder db = dbf.newDocumentBuilder(); 
		Document doc = db.parse(file);
		
		doc.getDocumentElement().normalize();
		parsePublicationPage(doc, PublicationPage.TAG, PublicationPage.ATTRIBUTES, file);
		
		}catch (Exception e){
			PublicationPage  page = new PublicationPage("ATENCION EL ARCHIVO" + file.getAbsolutePath()
					+ "ESTA CORRUPTO", "", "", "", "");
			System.out.println("");
			System.out.println("=============================");
			System.out.println("** ERROR: "+ e.getMessage());
			System.out.println("** FILE: " + file.getAbsolutePath());
			if (e.getMessage().contains("debe finalizar en el delimitador ';'")){
				System.out.println("====");
				System.out.println("** El error ha sido subsanado");
				System.out.println("** El contenido del archivo "+ file.getName()+ " ha sido modificado para ajustar un problema con entidades (&). Compruebe que el archivo de la salida sea correcto. ");
				parseSpecialAmpersan(file);
			}
			System.out.println("=============================");
			pages.add(page);
		}
	}
	
	
	private void parseSpecialAmpersan(File file){
		 // we need to store all the lines
	    List<String> lines = new ArrayList<String>();
	    String newLine ;
	    // first, read the file and store the changes
	    BufferedReader in;
	    
		try {
			in = new BufferedReader(new FileReader(file));
			 String line = in.readLine();
			   while (line != null) {
			    	newLine = line.replaceAll("&", "&amp;");
			        lines.add(newLine);
			        line = in.readLine();
			    }
			    in.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		    // now, write the file again with the changes
			try {
				PrintWriter out = new PrintWriter(file);
				for (String l : lines)
			        out.println(l);
			    out.close();
			        parseFile(file);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
		    
	   

	private void parsePublicationPage(Document doc, String tagName,
			String[] attributes, File file) {
		NodeList nList = doc.getElementsByTagName(tagName);
		for (int temp = 0; temp < nList.getLength(); temp++) {
			Node pageNode = nList.item(temp);
			if (pageNode.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) pageNode;
				ArrayList<String> attrList = new ArrayList<String>();
				for (int i = 0; i < attributes.length; i++) {
					attrList.add(eElement
									.getAttribute(PublicationPage.ATTRIBUTES[i]));
				}
				PublicationPage page = new PublicationPage(attrList.get(0),
						"\""+attrList.get(1)+ "\"", attrList.get(2), attrList.get(3),
						attrList.get(4));
				
				NodeList objectNList = eElement.getElementsByTagName(PublicationObject.TAG);
				for (int x = 0; x < objectNList.getLength(); x++) {
					Node objectNode = objectNList.item(x);
					if (objectNode.getNodeType() == Node.ELEMENT_NODE) {
						Element objectElement = (Element) objectNode;
						attrList = new ArrayList<String>();
						for (int i = 0; i < PublicationObject.ATTRIBUTES.length; i++) {
							attrList.add(objectElement
											.getAttribute(PublicationObject.ATTRIBUTES[i])
									);
						}
						PublicationObject object = new PublicationObject("\""
								+ attrList.get(0)+ "\"",
								attrList.get(1), attrList.get(2), attrList.get(3),
								attrList.get(4));
						NodeList itemNList = objectElement.getElementsByTagName(PublicationItem.TAG);
						for (int z = 0; z < itemNList.getLength(); z++) {
							Node itemNode = itemNList.item(z);
							if (itemNode.getNodeType() == Node.ELEMENT_NODE) {
								Element itemElement = (Element) itemNode;
								attrList = new ArrayList<String>();
								ArrayList<String> contentStringArray = new ArrayList<String>();
								for (int i = 0; i < PublicationItem.ATTRIBUTES.length; i++) {
									attrList.add("\""
											+ itemElement
													.getAttribute(PublicationItem.ATTRIBUTES[i])
											+ "\"");
									contentStringArray.add("\""
											+ itemElement.getTextContent()
											+ "\"");
								}
							
								PublicationItem item = new PublicationItem(attrList.get(0), contentStringArray.get(0), attrList.get(1).replace("\"", ""));
								object.addPublicationItem(item);
							}
						}
						page.addPublicationObect(object);
					}
				}
				pages.add(page);
			}
		}
	}

	public ArrayList<PublicationPage> getPages() {
		return pages;
	}
	
	
	
}
