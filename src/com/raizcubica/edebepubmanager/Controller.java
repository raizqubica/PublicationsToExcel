package com.raizcubica.edebepubmanager;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.output.TeeOutputStream;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.raizcubica.edebepubmanager.model.PublicationItem;
import com.raizcubica.edebepubmanager.model.PublicationObject;
import com.raizcubica.edebepubmanager.model.PublicationPage;

public class Controller {
	// Software basics
	private static final String FILE_TYPE = "xml";
	
	// Paths to files
	private static final String FILE_PATH = "/Users/victor/Desktop/pubs/publs_mex";
	private static final String EXIT_FILE_PATH = "/Users/victor/Desktop/pubs/exit";
	private static final String FILE_GENERATED_PATH = "Parsed";

	// Software functionalities
	private static boolean ACTIVATE_CSV_GENERATOR = false;
	private static boolean ACTIVATE_FILE_MAKER = true;
	private static int ONLY_ACT_IN_ONE_PUBLICATION = -1; //-1 to false (use all publications), other to set the publication position to act
	private static int LANGUAGE = 0 ; //0 Spanish, 1 Catalan, 2 Galician, 3 Basque 4 English

	// CSV Generator variables
	private static final String CSV_SEPARATOR = ",";
	private static final String QUOTES = "\"";
	ArrayList<String> csvTextLines;
	PublicationPage page;
	PublicationObject object;
	PublicationItem item;
	private HashMap<String, String> dictionaryPubNames;
	boolean isMex = true;

	ArrayList<ArrayList<PublicationPage>> arrayOfPublications; // Publications
																// orderer by
																// publication
																// Name in a
																// arrayList
	ArrayList<File> files;
	ArrayList<PublicationPage> pages;
	ArrayList<PublicationPage> pagesOrder;
	Document markupsDoc;
	Document resourcesDoc;

	// Counters
	int fileCounter = 0;
	int itemCounter = 0;
	String lastBookName = "";
	
	int objectCounter = 0;
	int videoCounter = 0;
	int audioCounter = 0;
	int linkCounter = 0;
	int imageCounter = 0; 
	int textCounter = 0;
	int pageCounter = 0;
	int htmlCounter = 0;
	int pdfCounter = 0;

	public static void main(String[] args) {
		Controller c = new Controller();
		c.run();

	}

	public Controller() {
	}

	public void run() {
		FileOutputStream fos;
		try {
			File destFile = new File(EXIT_FILE_PATH + File.separator + FILE_GENERATED_PATH);
			destFile.mkdirs();
			fos = new FileOutputStream(EXIT_FILE_PATH + File.separator + FILE_GENERATED_PATH + File.separator+ "Log --"+ new Date().toString() +".txt");
		    //we will want to print in standard "System.out" and in "file"
		    TeeOutputStream myOut=new TeeOutputStream(System.out, fos);
		    PrintStream ps = new PrintStream(myOut);
		    System.setOut(ps);
		    
		    start(); 
		    fos.close();
		} catch (Exception e) {
		    e.printStackTrace();
		} 
	}
	
	private void initObjectCounters(){
		objectCounter = 0;
		videoCounter = 0;
		audioCounter = 0;
		linkCounter = 0;
		imageCounter = 0; 
		textCounter = 0;
		pageCounter = 0;
		htmlCounter = 0;
		
	}

	private void start() {
		//Get the publication Names
		dictionaryPubNames = new HashMap<String, String>();
		FileWalker fw = new FileWalker();
		pagesOrder = new ArrayList<PublicationPage>();
		files = fw.getFilesByType(FILE_PATH, FILE_TYPE);

		PublicationXmlDomParser pxdp = new PublicationXmlDomParser();
		for (int i = 0; i < files.size(); i++) {
			//Cant be a directory
			//Only zips and xml
			if (!files.get(i).getAbsolutePath().contains("__MAC")){
				pxdp.parseFile(files.get(i));
				String pubF = files.get(i).getParentFile().getParentFile().getName();
				String pubN = pxdp.getPages().get(pxdp.getPages().size()==0?0:pxdp.getPages().size() - 1).getNamePub().replaceAll(QUOTES, "");
				dictionaryPubNames.put(pubN, pubF);
			}
		}
		pages = pxdp.getPages();

		if (ACTIVATE_CSV_GENERATOR)
			csvGenerator();
		makeArrayListOfPublications();
		fileStructureGenerator();
		//fillTheResources();
		makeTheMarkupsAndResourcesFile();
	}

	private String cleanString(String string) {
		return string.replace(QUOTES, "");
	}

	private void fileStructureGenerator() {
		for (int i = 0; i < this.arrayOfPublications.size(); i++) {
			
			ArrayList<PublicationPage> page1 = this.arrayOfPublications.get(i);
			PublicationPage page = page1.get(0);
			String publicationName = this.cleanString(page.getNamePub());
			String newPublicationName = this.dictionaryPubNames.get(publicationName);
			System.out.println(newPublicationName);
			File f = new File(FILE_PATH + File.separator + FILE_GENERATED_PATH + File.separator
					+ publicationName);
			if (isMex){
				f = new File(FILE_PATH + File.separator + FILE_GENERATED_PATH + File.separator
						+ newPublicationName);
			}
			if (!f.exists()) {
				try {
					
					
					f.mkdir();
					
					File res1 = new File(FILE_PATH + File.separator + FILE_GENERATED_PATH
							+ File.separator + publicationName + File.separator + "resources");

					File res2 = new File(FILE_PATH + File.separator + FILE_GENERATED_PATH
							+ File.separator + publicationName + File.separator + "resources" + File.separator
							+ "resources");
					if (isMex){
					res1 = new File(FILE_PATH + File.separator + FILE_GENERATED_PATH
							+ File.separator + newPublicationName + File.separator + "resources");

					res2 = new File(FILE_PATH + File.separator + FILE_GENERATED_PATH
							+ File.separator + newPublicationName + File.separator + "resources" + File.separator
							+ "resources");
					}


					res1.mkdir();
					res2.mkdir();

				} catch (SecurityException se) {
					se.printStackTrace();
				}
			}
		}
	}

	private void makeArrayListOfPublications() {
		HashSet<String> hs = new HashSet<String>();
		Iterator<PublicationPage> it = pages.iterator();
		while (it.hasNext()) {
			PublicationPage pp = it.next();
			if (pp.getNamePub() != null && pp.getNamePub() != "")
				hs.add(pp.getNamePub());
		}

		ArrayList<String> publicationsNameArray = new ArrayList<String>();
		publicationsNameArray.addAll(hs);

		this.arrayOfPublications = new ArrayList<ArrayList<PublicationPage>>();

		for (int i = 0; i < publicationsNameArray.size(); i++) {
			ArrayList<PublicationPage> publication = new ArrayList<PublicationPage>();
			this.arrayOfPublications.add(i, publication);
		}

		it = pages.iterator();
		while (it.hasNext()) {
			PublicationPage pp = it.next();
			for (int i = 0; i < publicationsNameArray.size(); i++) {
				if (pp.getNamePub().equals(publicationsNameArray.get(i))) {
					ArrayList<PublicationPage> pub = this.arrayOfPublications
							.get(i);
					pub.add(pp);
					this.arrayOfPublications.set(i, pub);
				}
			}
		}

		if (ONLY_ACT_IN_ONE_PUBLICATION >= 0) {
			ArrayList<PublicationPage> ppa = this.arrayOfPublications
					.get(ONLY_ACT_IN_ONE_PUBLICATION);
			this.arrayOfPublications = new ArrayList<ArrayList<PublicationPage>>();
			this.arrayOfPublications.add(ppa);
		}
	}

	private void makeTheMarkupsAndResourcesFile() {
		for (int i = 0; i < this.arrayOfPublications.size(); i++) {
			// Restart counters
			fileCounter = 0;
			initObjectCounters();
			
			markupsDoc = null;
			resourcesDoc = null;

			buildTheDocs();
			ArrayList<PublicationPage> pgArray = this.arrayOfPublications
					.get(i);
			for (int j = 0; j < pgArray.size(); j++) {
				JSONObject jsonPage = new JSONObject();
				// PAGE
				PublicationPage pp = pgArray.get(j);
				ArrayList<PublicationObject> poArray = pp
						.getPublicationObjects();

				for (int y = 0; y < poArray.size(); y++) {
					
					JSONObject jsonObject = new JSONObject();
					
					// PUBLICATION OBJECT
					PublicationObject po = poArray.get(y);
					ArrayList<PublicationItem> piArray = po
							.getPublicationItems();
					
					objectCounter++;
					
					String type = po.getType().replace(QUOTES, "");
					type = type.replace("Em", "");
					
					if (type.contains("link"))
						type = "url";
					if (type.equals("gallery"))
						type = "image";
					if (type.equals("internalLink")){
						type = "page";
					}
					
						
					
					//Fix markups behaviour
					String markup_type = type;
					if (!(type.equals("html1") && type.equals("audio") && type.equals("html1") && type.equals("text") && type.equals("video1") &&type.equals("page") || type.equals("url")))
					{
						if (type.equals("video")){
							markup_type = "video1";
						}else if (type.equals("image")){
							markup_type = "gallery";
						}else if (type.equals("html")){
							markup_type = "html1";
						}else if (type.equals("pdf")){
							//NEW
							markup_type = "file";
						}
					}
					
					//System.out.println("Markup Type = " + markup_type + "::: Type = " + type);
					// Se añade el markup a markupsDoc
					Element markups = (Element) markupsDoc.getFirstChild();
					Element markup = markupsDoc.createElement("markup");
					//markup.setAttribute("icon",
					//		po.getType().replaceAll(QUOTES, "").replace("Em", ""));
					markup.setAttribute("icon", markup_type);
					markup.setAttribute("id", "markup_" + objectCounter);
//					markup.setAttribute("page",
//							pp.getFolio().replaceAll(QUOTES, ""));
					markup.setAttribute("page", ""+j);
					markup.setAttribute("resourceId", "resource_"
							+ objectCounter);
					markups.appendChild(markup);
					
					

					
					try {
						jsonObject.put("markupId", "markup_" + objectCounter);
						jsonObject.put("page", j);
						//jsonObject.put("type", type.equals("icon")?"area":type);
						jsonObject.put("type", "area");
						Double[] poPositions = calculeValues(pp, po);
						jsonObject.put("x", poPositions[0]);
						jsonObject.put("y", poPositions[1]);
						jsonObject.put("w", poPositions[2]);
						jsonObject.put("h", poPositions[3]);
						jsonPage. append("", jsonObject);
					} catch (JSONException e) {
						
					}

	
					String title = makeTitle(type, ""+(j+1));

					Element resources = (Element) resourcesDoc.getFirstChild();
					Element resource = resourcesDoc.createElement("resource");
					resource.setAttribute("id", "resource_" + objectCounter);
					resource.setAttribute("title", title);
					resource.setAttribute("type", type);
					resource.setAttribute("value", "");

					
					
					for (int z = 0; z < piArray.size(); z++) {
						// PublicationItem
						PublicationItem pi = piArray.get(z);

						if (type.equals("url")) {
							resource.setAttribute("value",
									pi.getSrc().replace(QUOTES, ""));
						} else if (type.equals("audio")) {
							resource.setAttribute("value", "");
							Element item = (Element) resourcesDoc
									.createElement("item");
							item.setAttribute("duration", "0");
							item.setAttribute("id", "item_" + (z + 1));
							item.setAttribute("title", title);
							item.setAttribute("value",
									"resources"
											+ File.separator
											+ pi.getSrc().replace(QUOTES, "")
													.split(File.separator)[1]);
							resource.appendChild(item);
							if (piArray.size() > 1) {
								System.err
										.println("NUMERO DE AUDIOS EXCEDIDO - makeTheMarkupsAndResourcesFile()");
								System.exit(1);
							}
						} else if (type.equals("video")) {
							resource.setAttribute("value", "");
							Element item = (Element) resourcesDoc
									.createElement("item");
							item.setAttribute("duration", "0");
							item.setAttribute("height", "0");
							item.setAttribute("id", "item_" + (z + 1));
							item.setAttribute("subtitle", "");
							item.setAttribute("title", title);
							item.setAttribute("value",
									"resources"
											+ File.separator
											+ pi.getSrc().replace(QUOTES, "")
													.split(File.separator)[1]);
							item.setAttribute("witdth", "0");
							resource.appendChild(item);
							if (piArray.size() > 1) {
								System.err
										.println("NUMERO DE VIDEOS EXCEDIDO - makeTheMarkupsAndResourcesFile()");
								System.exit(1);
							}
						} else if (type.equals("text")) {
							resource.setAttribute("value", pi.getFooter()
									.replace(QUOTES, ""));
							if (piArray.size() > 1) {
								System.err
										.println("EL TEXTO NO PUEDE CONTENER ITEMS - makeTheMarkupsAndResourcesFile()");
								System.exit(1);
							}
						} else if (type.equals("image")
								|| type.equals("gallery")) {
							resource.setAttribute("value", "");
							Element item = (Element) resourcesDoc
									.createElement("item");
							item.setAttribute("witdth", "0");
							item.setAttribute("height", "0");
							item.setAttribute("id", "item_" + (z + 1));
							item.setAttribute("subtitle",
									pi.getFooter().replace(QUOTES, ""));
							item.setAttribute("value",
									"resources"
											+ File.separator
											+ pi.getSrc().replace(QUOTES, "")
													.split(File.separator)[1]);
							resource.appendChild(item);
						} else if (type.equals("html")) {
							resource.setAttribute("value",
									pi.getSrc().replace(QUOTES, "").split(File.separator)[1]);
							if (piArray.size() > 1) {
								System.err
										.println("EL HTML NO PUEDE CONTENER ITEMS - makeTheMarkupsAndResourcesFile()");
								System.exit(1);
							}
						} else if (type.equals("page")) {
							resource.setAttribute("value", pi.getFolio());
							if (piArray.size() > 1) {
								System.err
										.println("LA PAGINA NO PUEDE CONTENER ITEMS - makeTheMarkupsAndResourcesFile()");
								System.exit(1);
							}
						}else if (type.equals("pdf")) {
							resource.setAttribute("value",
									pi.getSrc().replace(QUOTES, "").split(File.separator)[1]);
							if (piArray.size() > 1) {
								System.err
										.println("EL HTML NO PUEDE CONTENER ITEMS - makeTheMarkupsAndResourcesFile()");
								System.exit(1);
							}
						} else {
							System.err
									.println("EL type "
											+ type
											+ " No está contemplado en el modelo de datos - makeTheMarkupsAndResourcesFile()");
							System.exit(1);

						}
						if (ACTIVATE_FILE_MAKER)
							copyFileToNewPath(pp, pi.getSrc().replace(QUOTES, ""));
					}
					resources.appendChild(resource);
				}
				buildMarkupXML(pp);
				buildResourcesXML(pp);
				
				try {
					buildTheJSON(jsonPage.getJSONArray("").toString(), pp, j);
				} catch (JSONException e) {
					JSONArray jsonA = new JSONArray();
					buildTheJSON(jsonA.toString(), pp, j);
				}
			}
		}
		

	}

	private void buildTheJSON(String string, PublicationPage pp, int jsonNumber) {
		try {
			FileUtils.write(new File(EXIT_FILE_PATH + File.separator + FILE_GENERATED_PATH
					+ File.separator + this.dictionaryPubNames.get(pp.getNamePub().replaceAll(QUOTES, ""))
					+ File.separator + "resources" + File.separator + jsonNumber +".json"), string, "UTF-8");
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	private String makeTitle(String type, String folio) {
		String objectCounterString = "" + objectCounter;
		if (type.equals("url")){
			linkCounter++;
			objectCounterString = "" + linkCounter;
		}else if (type.equals("audio")){
			audioCounter++;
			objectCounterString = "" + audioCounter;
		}else if (type.equals("text")){
			textCounter++;
			objectCounterString = "" + textCounter;
		}else if (type.equals("video")){
			videoCounter++;
			objectCounterString = "" + videoCounter;
		}else if (type.equals("image")){
			imageCounter++;
			objectCounterString = "" + imageCounter;
		}else if (type.equals("html")){
			htmlCounter++;
			objectCounterString = "" + htmlCounter;
		}else if (type.equals("page")){
			pageCounter++;
			objectCounterString = "" + pageCounter;
		}else if (type.equals("pdf")){
			pageCounter++;
			objectCounterString = "" + pdfCounter;
		}
		
		while (objectCounterString.length() < 3) {
			objectCounterString = "0" + objectCounterString;
		}

		if (type.equals("url")) {
			type = "Link";
		} else if (type.equals("audio")) {
			type = "Audio";
			
		} else if (type.equals("video")) {
			type = "Video";
			
		} else if (type.equals("text")) {
			type = "Texto";
			switch(LANGUAGE){
			case 1:
				type = "Text";
				break;
			case 3:
				type = "Testua";
				break;
			case 4:
				type = "Text";
				break;
			}
		} else if (type.equals("image")) {
			type = "Imágenes";
			switch(LANGUAGE){
			case 0:
				type = "Imágenes";
				break;
			case 1:
				type = "Imatges";
				break;
			case 2:
				type = "Imaxes";
				break;
			case 3:
				type = "Irudigintza";
				break;
			case 4:
				type = "Images";
				break;
			
			}
		} else if (type.equals("html")) {
			type = "Actividad";
			switch(LANGUAGE){
			case 0:
				type = "Actividad";
				break;
			case 1:
				type = "Activitat";
				break;
			case 2:
				type = "Actividade";
				break;
			case 3:
				type = "Jarduera";
				break;
			case 4:
				type = "Activity";
				break;
			
			}
		} else if (type.equals("page")) {
			type = "Link";
		} else if (type.equals("pdf")){
			type = "Pdf";	
		}
		
		String pageWord = "página";
		switch (LANGUAGE){
		case 0:
			pageWord = "página";
			break;
		case 1:
			pageWord = "pàgina";
			break;
		case 2:
			pageWord = "páxina";
			break;
		case 3:
			pageWord = "orria";
			break;
		case 4:
			pageWord = "page";
			break;
		
		}
		
		String resourceTitle = objectCounterString + " - " + type
				+ " - " + pageWord +" " + folio;
		return resourceTitle;
	}

	private void buildTheDocs() {
		if (markupsDoc == null) {
			markupsDoc = createDocument();
			Element markups = markupsDoc.createElement("markups");
			markupsDoc.appendChild(markups);

		}
		if (resourcesDoc == null) {
			resourcesDoc = createDocument();
			Element markups = resourcesDoc.createElement("resources");
			resourcesDoc.appendChild(markups);

		}
	}

	private String copyFileToNewPath(PublicationPage pp, String fileName) {
		try {
			fileName = java.net.URLDecoder.decode(fileName, "UTF-8");
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String originalResourcePath = FILE_PATH + File.separator
				+ this.dictionaryPubNames.get(pp.getNamePub().replace(QUOTES, "")) + File.separator + "src"
				+ File.separator + fileName.replace(QUOTES, "");
		if (isMex){
		 originalResourcePath = FILE_PATH + File.separator
				+ this.dictionaryPubNames.get(pp.getNamePub().replace(QUOTES, "")) + File.separator + "pages"
				+ File.separator + fileName.replace(QUOTES, "");
		}
		String destinationResourcePath = EXIT_FILE_PATH + File.separator + FILE_GENERATED_PATH
				+ File.separator + this.dictionaryPubNames.get(pp.getNamePub().replace(QUOTES, "")) + File.separator + "resources"
				+ File.separator;
		String fileNameDestination = fileName.replace(QUOTES, "");
		try{
			fileNameDestination = fileNameDestination.split(File.separator)[1];
		}catch (Exception e){
			System.out.println(fileName);
			System.out.println(fileNameDestination);
			System.out.println(e.getStackTrace());
		}
		File fOriginal = new File(originalResourcePath);
		if (!fileName.contains(".zip")) {
			File fDestination = new File(destinationResourcePath + "resources"
					+ File.separator + fileNameDestination);
			try {
				if (fOriginal.exists() && fOriginal.isFile()) {
					FileUtils.copyFile(fOriginal, fDestination);
					fileCounter++;
				}else{
					//patch 
					
					originalResourcePath = FILE_PATH + File.separator
							+ pp.getNamePub().replace(QUOTES, "") + File.separator + pp.getNamePub().replace(QUOTES, "") + File.separator + "src"
							+ File.separator + fileName.replace(QUOTES, "");
					if (isMex){
					originalResourcePath = FILE_PATH + File.separator
							+ pp.getNamePub().replace(QUOTES, "") + File.separator + pp.getNamePub().replace(QUOTES, "") + File.separator + "pages"
							+ File.separator + fileName.replace(QUOTES, "");
					}
					fOriginal = new File(originalResourcePath);
					if (fOriginal.exists() && fOriginal.isFile()) {
						FileUtils.copyFile(fOriginal, fDestination);
						fileCounter++;
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			File fDestination = new File(destinationResourcePath + fileNameDestination);
			try {
				if (fOriginal.exists()) {
					FileUtils.copyFile(fOriginal, fDestination);
					fileCounter++;
				}else{
					//patch 
					originalResourcePath = FILE_PATH + File.separator
							+ pp.getNamePub().replace(QUOTES, "") + File.separator + pp.getNamePub().replace(QUOTES, "") + File.separator + "src"
							+ File.separator + fileName.replace(QUOTES, "");
					if (isMex){
					originalResourcePath = FILE_PATH + File.separator
							+ pp.getNamePub().replace(QUOTES, "") + File.separator + pp.getNamePub().replace(QUOTES, "") + File.separator + "pages"
							+ File.separator + fileName.replace(QUOTES, "");
					}

					fOriginal = new File(originalResourcePath);
					if (fOriginal.exists() && fOriginal.isFile()) {
						FileUtils.copyFile(fOriginal, fDestination);
						fileCounter++;
					}
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	private static Document createDocument() {
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = null;
		Document doc = null;

		try {
			db = dbf.newDocumentBuilder();
			doc = db.newDocument();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		}
		return doc;
	}

	private void buildMarkupXML(PublicationPage page) {
		String markupString = DOM2String(markupsDoc);
		try {
			FileUtils.write(new File(EXIT_FILE_PATH + File.separator + FILE_GENERATED_PATH
					+ File.separator + this.dictionaryPubNames.get(page.getNamePub().replaceAll(QUOTES, ""))
					+ File.separator + "markups.xml"), markupString, "UTF-8");
		} catch (IOException e) {
			e.printStackTrace();
		}

		// }

	}

	private void buildResourcesXML(PublicationPage page) {
		// if (!this.lastBookName.equals(page.getNamePub())){
		String markupString = DOM2String(resourcesDoc);
		try {
			FileUtils.write(new File(EXIT_FILE_PATH + File.separator + FILE_GENERATED_PATH
					+ File.separator +this.dictionaryPubNames.get(page.getNamePub().replaceAll(QUOTES, ""))
					+ File.separator + "resources.xml"), markupString, "UTF-8");
		} catch (IOException e) {
			e.printStackTrace();
		}

		// }

	}

	public static String DOM2String(Document doc) {
		TransformerFactory transformerFactory = TransformerFactory
				.newInstance();
		Transformer transformer = null;
		try {
			transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION,
					"yes");
		} catch (javax.xml.transform.TransformerConfigurationException error) {
			return null;
		}

		Source source = new DOMSource(doc);

		StringWriter writer = new StringWriter();
		Result result = new StreamResult(writer);

		try {
			transformer.transform(source, result);
		} catch (javax.xml.transform.TransformerException error) {
			error.printStackTrace();
			return null;
		}

		String s = writer.toString();
		return s;
	}

	// CSV GENERATION

	private void csvGenerator() {

		ArrayList<String> csvTextLines = new ArrayList<String>();
		String csvLine = "Pub" + CSV_SEPARATOR + "Page" + CSV_SEPARATOR
				+ "Title" + CSV_SEPARATOR + "type" + CSV_SEPARATOR + "src"
				+ CSV_SEPARATOR + "Pie" + CSV_SEPARATOR + "srcWidth"
				+ CSV_SEPARATOR + "srcHeight" + CSV_SEPARATOR + "x1"
				+ CSV_SEPARATOR + "y1" + CSV_SEPARATOR + "x2" + CSV_SEPARATOR
				+ "y2";
		csvTextLines.add(csvLine);
		for (int i = 0; i < pages.size(); i++) {
			PublicationPage page = pages.get(i);
			PublicationObject object = null;
			PublicationItem item = null;

			if (page.getPublicationObjects().size() > 0) {
				for (int j = 0; j < page.getPublicationObjects().size(); j++) {
					object = page.getPublicationObjects().get(j);
					if (object.getPublicationItems().size() > 0) {
						for (int k = 0; k < object.getPublicationItems().size(); k++) {
							item = object.getPublicationItems().get(k);
							addCSVLine(csvTextLines, page, object, item);
						}
					} else {
						addCSVLine(csvTextLines, page, object,
								new PublicationItem("", "", ""));
					}
				}
			}
			try {
				FileUtils.writeLines(new File(FILE_PATH + "/publications.csv"),
						csvTextLines);
			} catch (IOException e) {
				e.printStackTrace();
			}

		}

	}

	private void addCSVLine(ArrayList<String> csvTextLines,
			PublicationPage page, PublicationObject object, PublicationItem item) {
		String csvLine;
		if (object != null && item != null) {

			if (this.checkLastElementPageAndObject(page, object)) {
				csvLine = page.getNamePub()
						+ CSV_SEPARATOR
						+ page.getFolio()
						+ CSV_SEPARATOR
						// Empty Title
						+ " " + CSV_SEPARATOR + object.getType()
						+ CSV_SEPARATOR + item.getSrc() + CSV_SEPARATOR
						+ item.getFooter() + CSV_SEPARATOR + page.getSrcWidth()
						+ CSV_SEPARATOR + page.getSrcHeight() + CSV_SEPARATOR
						+ object.getX1() + CSV_SEPARATOR + object.getY1()
						+ CSV_SEPARATOR + object.getX2() + CSV_SEPARATOR
						+ object.getY2();
			} else {
				csvLine = ""
						+ CSV_SEPARATOR
						+ ""
						+ CSV_SEPARATOR
						// Empty Title
						+ " " + CSV_SEPARATOR + "" + CSV_SEPARATOR
						+ item.getSrc() + CSV_SEPARATOR + item.getFooter()
						+ CSV_SEPARATOR + page.getSrcWidth() + CSV_SEPARATOR
						+ page.getSrcHeight() + CSV_SEPARATOR + object.getX1()
						+ CSV_SEPARATOR + object.getY1() + CSV_SEPARATOR
						+ object.getX2() + CSV_SEPARATOR + object.getY2();

			}
			csvTextLines.add(csvLine);
		}
	}

	private boolean checkLastElementPageAndObject(PublicationPage page,
			PublicationObject object) {
		if (page.equals(this.page) && object.equals(this.object)) {
			return false;
		}

		this.page = page;
		this.object = object;

		return true;

	}
	
	
	private Double[] calculeValues(PublicationPage pp , PublicationObject po){
		double pageWidth = Double.parseDouble(pp.getSrcWidth());
		double pageHeight = Double.parseDouble(pp.getSrcHeight());
		double x1 = Double.parseDouble(po.getX1().equals("")?"0":po.getX1());
		double x2 = Double.parseDouble(po.getX2().equals("")?"0":po.getX2());
		double y1 = Double.parseDouble(po.getY1().equals("")?"0":po.getY1());
		double y2 = Double.parseDouble(po.getY2().equals("")?"0":po.getY2());
		
		
		double w = (x2 - x1)/pageWidth;
		double h = (y2 - y1)/pageHeight;
		double x = x1/pageWidth;
		double y = y1/pageHeight;
		
		Double[] positionsOfPublicationObject = {x,y,w,h};	
		return positionsOfPublicationObject;
	}
}
