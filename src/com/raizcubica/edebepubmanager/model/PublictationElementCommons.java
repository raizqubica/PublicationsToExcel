package com.raizcubica.edebepubmanager.model;

public class PublictationElementCommons {
	public String tag;
	public String[] attributes;
	
	public PublictationElementCommons(String tag, String[] attributes) {
		super();
		this.tag = tag;
		this.attributes = attributes;
	}

	public String getTag() {
		return tag;
	}

	public String[] getAttributes() {
		return attributes;
	}
	
	
	
	
}
