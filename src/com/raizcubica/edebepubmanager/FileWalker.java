package com.raizcubica.edebepubmanager;

import java.io.File;
import java.util.ArrayList;


public class FileWalker {
	
	ArrayList<File> demandedFiles;
	public static int MAX_FILE_DEPTH = 3;

	public FileWalker() {
		demandedFiles = new ArrayList<File>();
	}

	public ArrayList<File> getFilesByType(String filePath, String fileType) {
		addFilesByType(filePath, fileType, 0);
		return demandedFiles;
	}

	private void addFilesByType(String filePath, String fileType, int depth) {
		if (depth < MAX_FILE_DEPTH){
			depth ++;
			File folder = new File(filePath);
			File[] listOfFiles = folder.listFiles();
			for (int i = 0; i < listOfFiles.length; i++) {
				if (listOfFiles[i].isFile()) {
					if (listOfFiles[i].getName().matches(
							"([^\\s]+(\\.(?i)(" + fileType + "))$)")) {
						demandedFiles.add(listOfFiles[i]);
					}
				} else if (listOfFiles[i].isDirectory() && depth != MAX_FILE_DEPTH) {
					addFilesByType(listOfFiles[i].getPath(), fileType, depth);
				}
			}
		}
		
	}

}