package com.raizcubica.edebepubmanager.model;

import java.util.ArrayList;

public class PublicationPage extends PublictationElementCommons{
	
	public static final String TAG = "page";
	public static final String[] ATTRIBUTES = {"folio", "namePub", "section", "srcWidth", "srcHeight"};
	
	String folio;
	String namePub;
	String section;
	String srcWidth;
	String srcHeight;
	ArrayList<PublicationObject> publicationObjects;
	
	public PublicationPage(String folio, String namePub, String section, String srcWidth,
			String srcHeight) {
		super(TAG, ATTRIBUTES);
		this.folio = folio;
		this.namePub = namePub;
		this.section = section;
		this.srcWidth = srcWidth;
		this.srcHeight = srcHeight;
		publicationObjects = new ArrayList<PublicationObject>();
	}
	
	public String getFolio() {
		return folio;
	}
	public void setFolio(String folio) {
		this.folio = folio;
	}
	public String getNamePub() {
		return namePub;
	}
	public void setNamePub(String namePub) {
		this.namePub = namePub;
	}
	public String getSection() {
		return section;
	}
	public void setSection(String section) {
		this.section = section;
	}
	public String getSrcWidth() {
		return srcWidth;
	}
	public void setSrcWidth(String srcWidth) {
		this.srcWidth = srcWidth;
	}
	public String getSrcHeight() {
		return srcHeight;
	}
	public void setSrcHeight(String srcHeight) {
		this.srcHeight = srcHeight;
	}
	
	public ArrayList<PublicationObject> getPublicationObjects() {
		return publicationObjects;
	}

	public void setPublicationObjects(
			ArrayList<PublicationObject> publicationObjects) {
		this.publicationObjects = publicationObjects;
	}
	
	public void addPublicationObect(PublicationObject publicationObject){
		this.publicationObjects.add(publicationObject);	
	}

	@Override
	public String toString() {
		return "PublicationPage [folio=" + folio + ", namePub=" + namePub
				+ ", section=" + section + ", srcWidth=" + srcWidth
				+ ", srcHeight=" + srcHeight + ", publicationObjects="
				+ publicationObjects + "]";
	}
	
	
	
	
}
