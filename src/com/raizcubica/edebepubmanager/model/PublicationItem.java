package com.raizcubica.edebepubmanager.model;

public class PublicationItem extends PublictationElementCommons{
	
	public static final String TAG = "item";
	public static final String[] ATTRIBUTES = {"src", "folio"};
	
	String src;
	String footer;
	String folio;

	public PublicationItem(String src, String footer, String sheet) {
		super(TAG, ATTRIBUTES);
		this.src = src;
		this.footer = footer;
		this.folio = sheet;
	}

	public String getFolio() {
		return folio;
	}

	public void setFolio(String folio) {
		this.folio = folio;
	}

	public String getSrc() {
		return src;
	}

	public void setSrc(String src) {
		this.src = src;
	}
	
	public String getFooter() {
		return footer;
	}

	public void setFooter(String footer) {
		this.footer = footer;
	}

	@Override
	public String toString() {
		return "PublicationItem [src=" + src + " " + "footer=" + footer + "]";
	}

	

}
