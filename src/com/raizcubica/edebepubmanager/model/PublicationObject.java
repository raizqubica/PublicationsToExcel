package com.raizcubica.edebepubmanager.model;

import java.util.ArrayList;

public class PublicationObject extends PublictationElementCommons{
	
	public static final String TAG = "object";
	public static final String[] ATTRIBUTES = {"type", "x1", "x2", "y1", "y2"};

	String type;
	String x1;
	String x2;
	String y1;
	String y2;
	ArrayList<PublicationItem> publicationItems;

	public PublicationObject(String type, String x1, String x2, String y1,
			String y2) {
		super(TAG, ATTRIBUTES);
		this.type = type;
		this.x1 = x1;
		this.x2 = x2;
		this.y1 = y1;
		this.y2 = y2;
		publicationItems = new ArrayList<PublicationItem>();
	}
	
	

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getX1() {
		return x1;
	}

	public void setX1(String x1) {
		this.x1 = x1;
	}

	public String getX2() {
		return x2;
	}

	public void setX2(String x2) {
		this.x2 = x2;
	}

	public String getY1() {
		return y1;
	}

	public void setY1(String y1) {
		this.y1 = y1;
	}

	public String getY2() {
		return y2;
	}

	public void setY2(String y2) {
		this.y2 = y2;
	}


	public ArrayList<PublicationItem> getPublicationItems() {
		return publicationItems;
	}


	public void setPublicationItems(ArrayList<PublicationItem> publicationItems) {
		this.publicationItems = publicationItems;
	}
	
	public void addPublicationItem(PublicationItem publicationItem){
		this.publicationItems.add(publicationItem);
	}



	@Override
	public String toString() {
		return "PublicationObject [type=" + type + ", x1=" + x1 + ", x2=" + x2
				+ ", y1=" + y1 + ", y2=" + y2 + ", publicationItems="
				+ publicationItems + "]";
	}

	

	
	
}
